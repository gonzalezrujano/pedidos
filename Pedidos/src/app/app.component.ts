import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from './animations';
import { AutenticacionService } from './services/autenticacion/autenticacion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
  	slideInAnimation
  ]
})

export class AppComponent {
  title = 'Pedidos';

   /**
   * Almacena el objeto obtenido de la API
   *
   * @property {object}  
   **/
   obtener;

   /**
   * Almacena el Token obtenido
   *
   * @property {object}  
   **/
   token: string;

   constructor(private autenticacionService: AutenticacionService){
    this.autenticacionService.obtenerTokenApi().subscribe(
      res => {
        this.obtener = res;
        this.token = this.obtener.token;
         this.autenticacionService.guardarToken(this.token);
      },
      err => {
        console.log(err);
      }
    )
   }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  ngAfterContentInit(){
  
  }
}
