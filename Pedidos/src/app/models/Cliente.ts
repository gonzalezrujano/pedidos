import { Sucursal } from './Sucursal';
import {Horario} from './Horario';
export class Cliente {	

	constructor(
		public id: number,
		public name: string,
		public sucursal: Sucursal[],
		public horario: Horario[]
	){}
}