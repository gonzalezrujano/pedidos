import { Cliente }   from './Cliente';
import { MetodoPago } from './MetodoPago';
import { EstadoPago } from './EstadoPago';
import { Sucursal } from './Sucursal';
import { Horario } from './Horario';
import { Producto } from './Producto';
import { Comentario } from './Comentario';

export class Pedido {
	
	constructor(
		public id: number,
		public norden: string,
		public usuario: Cliente,
		public metodoPago: MetodoPago,
		public estado_pago: any,
		public medio_pago: any,
		public estadoPago: EstadoPago,
		public sucursal: Sucursal,
		public horario: Horario,
		public fecha_entrega: string,
		public total: number,
		public productos: Producto[],
		public comentarios: Comentario[]
	){}
}