export class EstadoPago {

	constructor(
		public id: number,
		public estado: string
	){}
}