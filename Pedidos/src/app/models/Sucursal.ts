import { Direccion } from './Direccion';

export class Sucursal {	

	constructor(
		public id: number,
		public nombre: string,
		public ubicacion: string,
		public direccion: any
	){}
}