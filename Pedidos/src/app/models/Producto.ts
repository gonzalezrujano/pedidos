export class Producto {	

	constructor(
		public id: number,
		public nodoproducto: [],
		public avatar: string,
		public nombre: string,
		public unidadCompra: string,
		public cantidad: number,
		public precioUnidad: number,
		public subtotal: number
	){}
}