import { Pedido } from '../models/Pedido';
import { Cliente } from '../models/Cliente';
import { Sucursal } from '../models/Sucursal';
import { Direccion } from '../models/Direccion';
import { MetodoPago } from '../models/MetodoPago';
import { EstadoPago } from '../models/EstadoPago';
import { Horario } from '../models/Horario';
import { Producto } from '../models/Producto';
import { Comentario } from '../models/Comentario';

/** 
 * Objeto de los pedidos que tiene un cliente
 * 
 * @return {Object}
*/
import { Injectable } from '@angular/core';

@Injectable()
export class pedidos {
  // pedidosPrueba: Array<Pedido>  = [];
  pedidos: any= [];
  datosClienteGlobal: any = 0;
  datosCargados: boolean = false;
  static creditos: any = [];
}