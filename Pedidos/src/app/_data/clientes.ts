import { Cliente } from '../models/Cliente';

    /**
   * Dato de la api de tipo Cliente
   * 
   * @property {Cliente}
   */    
   export const clientes: Array<Cliente> = [];
