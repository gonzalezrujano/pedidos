import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AutenticacionInterceptor } from './services/autenticacion.interceptor';

import { AppRoutingModule } from './app-routing.module'; 
import { AppComponent } from './app.component';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { SeparadorPipe } from './_pipe/separador.pipe';
import { Filtro } from './_pipe/filter.pipe';
import { FormsModule } from '@angular/forms';
import { pedidos } from './_data/pedidos';
import { NgSelectModule } from '@ng-select/ng-select';

//componentes
import { NavBarComponent } from './componentes/nav-bar/nav-bar.component';
import { ComentariosComponent } from './componentes/pedido/comentarios/comentarios.component';
import { PrecargaComponent } from './componentes/precarga/precarga.component';
import { GestionProductosComponent } from './componentes/pedido/gestion-productos/gestion-productos.component'
import { CartaPedidoComponent } from './componentes/home/carta-pedido/carta-pedido.component';
import { ResumenDiaComponent } from './componentes/home/resumen-dia/resumen-dia.component';
import { FormPedidosComponent } from './componentes/pedido/form-pedidos/form-pedidos.component';
import { ListPedidosComponent } from './componentes/home/list-pedidos/list-pedidos.component';

@NgModule({
  declarations: [
    AppComponent,
    FormPedidosComponent,
    ListPedidosComponent,
    NavBarComponent,
    CartaPedidoComponent,
    ResumenDiaComponent,
    SeparadorPipe,
    Filtro,
    GestionProductosComponent,
    ComentariosComponent,
    PrecargaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  providers: [
    pedidos,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AutenticacionInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
