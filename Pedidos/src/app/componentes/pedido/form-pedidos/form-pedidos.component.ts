import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { pedidos } from '../../../_data/pedidos';
import { ClientesService } from '../../../services/cliente/clientes.service';
import { PedidosService } from '../../../services/pedido/pedidos.service';

// Modelos de datos
import { Producto } from '../../../models/Producto';


// Declaracion de variable Materialize
declare var M: any;

@Component({
  selector: 'app-form-pedidos',
  templateUrl: './form-pedidos.component.html',
  styleUrls: ['./form-pedidos.component.scss']
})

export class FormPedidosComponent implements OnInit {
  tipoDeFormulario: boolean = false;
  /**
  * Almacena el credito del cliente
  *
  * @property {number}
  */  
  creditos

  /**
  * Almacena el n° de Orden
  *
  * @property {FormGroup}
  */
  norden: string = '';


   /**
  * Almacena el n° de Cliente
  *
  * @property {FormGroup}
  */
  idCliente: number = -1;

  /**
  * Almacena el n° de Pedido
  *
  * @property {FormGroup}
  */
  idPedido: number ;

  /**
  * Formulario para almacenar datos del pedido
  *
  * @property {FormGroup}
  */
  datos_pedido: FormGroup;

  /**
  * Esta enviado el formulario de los datos del pedido
  *
  * @property {boolean}
  */
  submitted: boolean = false; 

 
   /**
  * Formulario para almacenar datos del pedido
  *
  * @property {boolean}
  */
  edit: boolean = false;

  /**
  * Valor del campo fecha de entrega
  *
  * @property {string}
  */
  valorCampoFechaEntrega: string;
  
  /**
  * Valor del campo fecha de entrega
  *
  * @property {Object}
  */
  creditoSobrante: any = {data : 0, type: 0};

  pedido: any = [];
  clientes: any = [];
  productosDelPedido: any = [];
  horarios: any = [];
  sucursales: any = [];
  Obtenernorden: any = [];

  /**
  * Listado de productos del pedido
  *
  * @property {string}
  */
  listProducts: Array<Producto> = [];

  constructor(
    private fb: FormBuilder, 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private clientesService: ClientesService,
    private pedidosService: PedidosService,
    private Globals: pedidos
  ) 
  {
    const params = this.activatedRoute.snapshot.params;
    this.idPedido = params.id;
    if(params.id){//Editar pedido
      this.pedidosService.getObtenerProductosPedido(params.id).
      subscribe(res => {        
        let pedido = this.Globals.pedidos.data.find((pedido) => (pedido.id == params.id))
        let id = pedido.usuario.id
        this.idCliente = parseInt(id);
        this.productosDelPedido = res;
        this.serializarPrecioProductos()
        this.mostrarCampos(parseInt(id))
      }, err => {
        console.error(err);
      })
    }else{ //Crear pedido
      this.obtenerClientes();
    } 
  }


  serializarPrecioProductos(){
    this.productosDelPedido.data.map( producto =>{
      producto.precio = producto.nodoproducto.maestroprecio[0].precio;
      producto.precio = { precio :producto.nodoproducto.maestroprecio[0].precio}
    })
  }
   
  ngAfterViewInit(){
    var elemss = document.querySelectorAll('.datepicker');    
    M.Datepicker.init(elemss, {
      firstDay: true, 
        format: 'dd-mm-yyyy',
        i18n: {
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
            weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
            weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"]
        },
        minDate: new Date()});
  }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    //Actualizar headertitle
    if(params.id)   
      this.edit = true;   
    //Crear formData  
    this.datos_pedido = new FormGroup({
      id: new FormControl(''),
      nOrden: new FormControl(''),
      cliente: new FormControl(null, Validators.required),
      fechaEntrega: new FormControl('', Validators.required),
      horario: new FormControl('', Validators.required),
      sucursal: new FormControl('', Validators.required),
      direccion: new FormControl(''),
      direccion_id: new FormControl(''),
    });
  
  }

  serializarProductosDelPedido(){
    // console.log("resilaea",this.productosDelPedido)
    this.productosDelPedido.data.map(element=>{
      
      element.id = element.id
		  element.avatar = element.nodoproducto.materiaproductos.imagen
		  element.nombre = element.nodoproducto.materiaproductos.nombre
		  element.unidadCompra = element.nodoproducto.materiaproductos.unidad_compra
		  element.cantidad = element.cantidad
		  element.precioUnidad = element.precio.precio
		  element.subtotal = element.total
    })
  }
  
  /** 
  * Obtiene el cliente con los datos de sucursales, horario
  * del cliente seleccionado.
  * 
  * @view Crear Pedido
  * @return {object}  
  **/
  obtenerClientes(){
    this.clientesService.obtenerClientes().subscribe(
      res => {
        this.clientes = res;
        this.initRenderizarSelect();
      },
      err => {
        console.log("Error", err);
      }
    )
  }

  /**
  * Agrega el scroll al boton desplegable
  *
  * @return {void}  
  **/
  activarScrollEnDesplegable() 
  {
   document.getElementsByClassName('ng-dropdown-panel-items scroll-host')[0].setAttribute('style', 'height: 80px;');
  }
  
  /**
  * Obtiene el n°orden, los horarios y sucursales del cliente seleccionado
  * 
  * @view Crear pedido
  * @return {object}  
  **/
  obtenerCampos(id_cliente){
    this.pedidosService.obtenernOrden(id_cliente.value).subscribe(
      res => {
         this.idCliente =  id_cliente.value;
         this.f.nOrden.reset('');
         //this.f.horario.reset('');
         this.f.sucursal.reset('');
         this.norden = '';
         this.Obtenernorden = res;
         this.norden = this.Obtenernorden.norden;
         this.datos_pedido.patchValue({
            nOrden: this.norden 
         }) 
      },
      err => {
        console.log(err);
      });

    this.clientes.data.filter((element) => {   
      if(element.id == id_cliente.value){
        this.horarios = element.horarios;
        // Validar sucursales
        if(typeof(element.sucursales) == "string"){
          this.sucursales = [];
        }else{
          this.sucursales = element.sucursales;
        }
        this.creditos = element.creditos
        this.pedidosService.saveCreditos(element.creditos)
        this.initRenderizarSelect()
      }
    })
    this.datos_pedido.patchValue({
      fechaEntrega: this.formatFechaYYYY(new Date())
    }) 
  }

  /**
  * Obtiene el n°orden, los horarios y sucursales del cliente seleccionado
  * 
  * @view Show pedido
  * @return {object}  
  **/
  mostrarCampos(id_cliente)
  {
    let pedido = this.Globals.pedidos.data.find((pedido) => (pedido.usuario.id == id_cliente))
    this.norden = pedido.norden;

    this.datos_pedido.patchValue({
      nOrden : pedido.norden,
      cliente: pedido.usuario.name,
      fechaEntrega: pedido.fecha_entrega,
      horario: pedido.horario.id,
      sucursal: pedido.sucursal.id,
      direccion_id: pedido.sucursal.direccion.id, 
      direccion: pedido.sucursal.direccion.direccion 
    })
    // this.compararFechas()

    this.clientesService.obtenerClientes().subscribe(
      res => {
        this.clientes = res;
        this.clientes.data.filter((element) => {          
          // console.log("s->>>", element)
          if(element.id == id_cliente){
            this.horarios = element.horarios;
            this.sucursales = element.sucursales;    
            this.creditos = element.creditos
            this.pedidosService.saveCreditos(element.creditos)    
          }
        })
        this.initRenderizarSelect();
      },
      err => {
        console.log(err);
      }
    )   
  }
  
  /**
  * Obtiene la dirección de la sucursal seleccionada
  * 
  * @view in HTML
  * @return {string}  
  **/
  obtenerDireccion(sucursal_id){
    this.f.direccion.reset('');

    this.sucursales.filter((element) => {
       if(element.id == sucursal_id.value){
        //  console.log("sucurusale", element)
         this.datos_pedido.patchValue({
             direccion_id: element.direccion.id, 
             direccion: element.direccion.direccion
         }) 
       }
    }) 
  }

  /**
  * Obtiene el valor de la fecha de entrega
  *
  * @view in HTML
  * @return {String}  
  **/
  obtenerFechaEntrega(e){
    var campoFechaEntrega = document.getElementById('fecha');
    this.valorCampoFechaEntrega = M.Datepicker.getInstance(campoFechaEntrega).toString();
    // console.log("valorCampoFechaEntrega___", this.valorCampoFechaEntrega.split('-'))
    // if(this.valorCampoFechaEntrega=="")
    this.f.fechaEntrega.setValue( this.formatFechaEnvio(this.valorCampoFechaEntrega) );
  }
  
  /**
  * Renderizar elementos select luego de obtener los datos
  * para el blucle ngfor.
  *  
  * @view Crear y Show pedido
  * @return {void}
  **/
  initRenderizarSelect(){
    setTimeout(function(){
      var elems = document.querySelectorAll('select');
      M.FormSelect.init(elems);
    })
  }

  /**
   * Get para obtener los datos del FormData creado
   *
   *  @view in HTML
   * @return {FormData}
   */
  get f()
  { 
    return this.datos_pedido.controls; 
  }  

  /**
  * Almacenar datos generales del pedido
  * 
  * @return {Object}  
  **/
  SavePedido(){
    let data;
    this.submitted = true;
    data = this.datos_pedido.value
    data.productos = this.productosDelPedido.data
    // console.log("datos-pedido", data)
    this.serializeSendClientArray(data);
    this.serializeSendProductArray(data.productos, data);
    // console.log("productos", this.productosDelPedido)
    // console.log("DAedit", data )
    if(this.datos_pedido.invalid || this.productosDelPedido.data.length <= 0)
    {
      return;
    }else{
      if(!this.edit){
        // console.log("DAedit", data )
        if(this.creditos < data.total){
          let toastHtml= "<span>No posee creditos suficientes</span>"
          M.toast({html: toastHtml})
        }else{
          this.ServiceCreatePedido(data)
        }    
      }else{
        if(this.creditos < this.creditoSobrante.data){
          let toastHtml= "<span>No posee creditos suficientes</span>"
          M.toast({html: toastHtml})
        }else{
          this.ServiceUpdatePedido(data)
        }           
      }
    }
  }

  /**
   * Llamada a la api para actualizar un pedido
   * @param data 
   */
  ServiceUpdatePedido(data){
    this.pedidosService.updatePedido(data.id_pedido, data).subscribe(res=>{
      // console.log("createee", res)
      let toastHtml= "<span>Pedido actualizado correctamente</span>"
      M.toast({html: toastHtml})
      setTimeout(() => {
        this.router.navigateByUrl('pedidos');  
      }, 1000);
      
    }, err => {
      let toastHtml= "<span>Ocurrio un error al tratar de actualizar el pedido</span>"
      M.toast({html: toastHtml})
    })
  }

  /**
   * Llamada a la api para Crear un nuevo pedido
   * @param data 
   */
  ServiceCreatePedido(data){
    // console.log(data);
    this.pedidosService.createPedido(data).subscribe(res=>{      
      let toastHtml= "<span>Pedido creado correctamente</span>"
      M.toast({html: toastHtml})
      setTimeout(() => {
        this.router.navigateByUrl('pedidos');  
      }, 1000);
    }, err=> {
      let toastHtml= `<span>Ocurrio un error: ${err.error.errors.fecha_entrega[0]}</span>`
      M.toast({html: toastHtml})
      // console.log(err)
    })
  }

  /**
   * Serializa los datos del formulario a mostrar en la cabecera.
   * @param data 
   */
  serializeSendClientArray(data){
    data.id =  this.idPedido;
    data.id_pedido =  this.idPedido;
    data.user_id = this.idCliente
    data.horario_id = parseInt(data.horario)
    data.direccion_id = data.direccion_id
    data.norden = data.nOrden
    data.fecha_entrega = data.fechaEntrega
    data.total = 0
  }

  /**
   * Renderiza los datos para mostraren la ficha de lista de productos
   * @param data 
   * @param cliente 
   */
  serializeSendProductArray(data, cliente){
    data.map(element =>{
      if(this.edit){
        if ( element.nodoproducto.id == undefined){        
        element.nodoproducto_id = element.id 
        }else{
        element.nodoproducto_id = element.nodoproducto.id
        }
      }else{
        element.nodoproducto_id = parseInt(element.id)
      }      
      element.subtotal = parseFloat(element.total)
      cliente.total += parseFloat(element.total);
    })
    cliente.total+= Math.round(cliente.total*0.19)
  }

  // /**
  // * Apartir de un dato de tipo Date, se retorna un string formateado a 'dd-MM-YYYY'.
  // *
  // * @param date Un dato de tipo Object-Date, que contiene una fecha
  // * @return string
  // */
  // public formatFecha(fecha){
  //   return(fecha.getDate()<10?'0':'')+fecha.getDate()+'-'+(fecha.getMonth()<10?'0':'')+(fecha.getMonth() + 1)+'-'+fecha.getFullYear()
  // }

  /**
  * Apartir de un dato de tipo String, se retorna un string formateado a 'YYYY-MM-dd'.
  *
  * @param String Un dato de tipo Object-Date, que contiene una fecha
  * @return string
  */
  public formatFechaEnvio(fecha){ 
    let data_fecha = fecha.split('-')
    
    return data_fecha[2]+'-'+data_fecha[1]+'-'+data_fecha[0];
  }


  /**
   * ya no esta en uso
   */
  public compararFechas(){
    let fecha_Actual = this.formatFechaYYYY(new Date())    
    this.tipoDeFormulario = !(this.Globals.pedidos.data[0].fecha_entrega >= fecha_Actual)  
  }

  /**
  * Apartir de un dato de tipo Date, se retorna un string formateado a 'YYYY-MM-dd'.
  *
  * @param date Un dato de tipo Object-Date, que contiene una fecha
  * @return string
  */
  public formatFechaYYYY(fecha){
    return fecha.getFullYear()+'-'+(fecha.getMonth()<10?'0':'')+(fecha.getMonth() + 1)+'-'+(fecha.getDate()<10?'0':'')+fecha.getDate()
  }
}