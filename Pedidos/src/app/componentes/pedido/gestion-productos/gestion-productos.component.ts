import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//models
import { Pedido } from '../../../models/Pedido';
import { Producto } from '../../../models/Producto';
//services
import { ProductosService} from '../../../services/producto/productos.service'
// datos
import { products } from '../../../_data/products';
import { IMG_URI } from '../../../services/apiUrl';
import { PedidosService } from '../../../services/pedido/pedidos.service';
declare var M: any;
@Component({
  selector: 'app-gestion-productos',
  templateUrl: './gestion-productos.component.html',
  styleUrls: ['./gestion-productos.component.scss']
})
export class GestionProductosComponent implements OnInit {

  @Input('productosDelPedido') productosDelPedido: any;
  @Input('idCliente') idCliente: number;
  @Input('tipoDeFormulario') tipoDeFormulario: boolean;
  @Input('creditos') creditos: number;
  @Input('creditoSobrante') creditoSobrante;

  //@Input('habilitarEdicion') habilitarEdicion: boolean;
  /**
   * Almacena el total de un pedido antes de editarlo
   * 
   * @property {numberç>}
   */  
  totalPedidoEdit: number;

  /**
   * Evento de envio de formulario
   * 
   * @property {EventEmitter<boolean>}
   */
  @Output() enviarFormulario = new EventEmitter<boolean>(); 
  
  /**
  * Listado de productos en inventario
  *
  * @property {Array<Producto>}
  */
  listProductsInventary: Array<Producto>;

  /**
  * Listado de productos del pedido
  *
  * @property {Array<Producto>}
  */
  listProducts: Array<any>;

  /**
  * Importe bruto del pedido sin IVA
  *
  * @property {number}
  */
  subtotal: number = 0;

  
  // /**
  // * Almacena creditos disponibles del cliente
  // *
  // * @property {number}
  // */
  // creditos: number = 0;

  /**
  * IVA del total del pedido
  *
  * @property {number}
  */
  iva: number = 19;


  /**
  * IVA del total del pedido
  *
  * @property {number}
  */
  totalIva: number;


  /**
  * Importe bruto del pedido
  *
  * @property {number}
  */
  totalPedido: number = 0;

  /**
  * Formulario de gestion de productos
  *
  * @property {FormGroup}
  */
  newProduct: FormGroup;

  /**
  * Esta enviado el formulario
  *
  * @property {boolean}
  */
  submitted2: boolean = false;

  /**
  * Comprueba si el producto es maestro
  *
  * @property {boolean}
  */
  maestroProducto: boolean = false;

  /**
  * Atributos para ocultar formulario
  *
  * @property {boolean}
  */
  disabledClass : boolean;

  lengthListProducts : number;
  valorFiltro : string;
  showFiltro : boolean;
  nodo_id
  /**
  * Url de las imagenes de los productos - Caso: Materia Productos 
  *
  * @property {string}
  */
  urlMateriaProductos: string = `${IMG_URI}/storage/img_materiaproductos/`;

  /**
  * Url de las imagenes de los productos - Caso: Maestro Productos 
  *
  * @property {string}
  */
  urlMaestroProductos: string = `${IMG_URI}/storage/imagenes_productos/`;

  
  /**
  * Lista de productos
  *
  * @property {Array<any>}
  */
  listProductos

  /**
  * Tipo de formularo Create = False, Update True
  *
  * @property {boolean}
  */
  TypeFormulario : boolean = false;

  /**
   * Eventos de incremento de inputNumber
   * 
   * @return {void}
  */  
  public inputIncrement(i){
    if(!this.tipoDeFormulario){
    this.listProducts[i].cantidad ++;
    this.calculePricesProduct(this.listProducts[i].cantidad, i)}
  }  

  /**
   * Eventos de decremento de inputNumber
  * 
  * @return {void}
 */  
  public inputDecrement(i){
    if(!this.tipoDeFormulario){
    this.listProducts[i].cantidad  <= 0 ? 0 : this.listProducts[i].cantidad--
    this.calculePricesProduct(this.listProducts[i].cantidad, i)}
  }
  /**
     * Eventos de incremento de inputNumber de nuevo Producto
     * 
     * @return {void}
    */  
  public inputIncrementNewProduct(){
    
    this.f2.cantidad.setValue(this.f2.cantidad.value+1)  
  }  

  /**
  * Eventos de decremento de inputNumber de nuevo Producto
  * 
  * @return {void}
  */  
  public inputDecrementNewProduct(){
    var decremento = this.f2.cantidad.value;

    decremento = decremento <= 1 ? 1 : decremento-=1  
    this.f2.cantidad.setValue(  decremento )
  }
  /**
   * Calcular el Subtotal de un producto 
   * 
   * @return {void}
  */
  public calculePricesProduct(productCantidad, i){    
    this.listProducts[i].total = parseFloat(productCantidad) * this.listProducts[i].precio.precio; 
    this.calculeSumSubTotal()
    this.compararCreditosAlEditar()
  }

  /**
   * Calcular el Total del pedido
   * 
   * @return {void}
  */
  public calculeSumSubTotal(){
    this.subtotal = 0;
    this.listProducts.map((producto) => {
      this.subtotal += producto.total;
    });
    this.totalIva = Math.round(parseFloat(this.subtotal.toString()) * (this.iva / 100) )
    this.totalPedido = this.subtotal +  this.totalIva;
    return this.totalPedido;
  }

  /**
  * Almacenar producto
  * 
  * @return {Productos}  
  **/
  public addProduct()
  {
    var newProduct : any;
    var i;
    this.submitted2 =true;
    if(this.newProduct.invalid){
      return;
    }
  
    newProduct = new Producto(null, [], "", "", "", 0, 0, 0);
    this.disabledClass = true
    this.showFiltro = true;
    this.lengthListProducts = this.lengthListProducts + 1;
    var subtotalProducto = this.newProduct.value.precioUnidad * this.newProduct.value.cantidad;
    this.newProduct.patchValue({ subtotal: subtotalProducto });
    
    Object.assign(newProduct, this.returnProductSerializado());
    // console.log("newProductnewProduct", newProduct)
    this.listProducts.push(newProduct);
    
    i = this.listProducts.length - 1;

    this.calculePricesProduct(this.listProducts[i].cantidad, i)
    // console.log("Esta es la lista de productos: ", this.listProducts);
  }

  /**
   * Eliminar un producto de la lista de productos del pedido
   * 
   * @return {void}
  */
  public deletedProduct(id){
    
    this.lengthListProducts -= 1;
    this.listProducts.splice(id, 1)
    this.calculeSumSubTotal()  
    this.compararCreditosAlEditar()  
  }

  /**
   * Compra al editar que los creditos sean los suficientes
   * para editar el pedido.
   * 
   * @return {void}
  */
  public compararCreditosAlEditar(){
    let tipoDeEdicion = this.totalPedidoEdit- this.calculeSumSubTotal()
    let temp = 0
    // Si no se esta editando no comparar
    if (!this.TypeFormulario)
    return ;

    if(tipoDeEdicion < 0)
    { // Se esta agregando productos
      temp = (-1)*tipoDeEdicion
      this.creditoSobrante.type = -1
    }
    else{
      this.creditoSobrante.type = 1
      temp = tipoDeEdicion
    }    
    this.creditoSobrante.data = temp
  }
  

  /**
   * Mostrar el campo para filtrar la lista de productos
   * 
   * @return {void}
  */
  public verFiltro() : void{
    this.showFiltro = !this.showFiltro;
    this.disabledClass = true
  }

  /**
   * Ocultar y limpiar el campo para filtrar la lista de productos
   * 
   * @return {void}
  */
  public CancelarFiltro() : void{
   this.verFiltro()
   this.valorFiltro = "" 
  }

  get f2()
  { 
  	return this.newProduct.controls; 
  }  

  constructor(private fb: FormBuilder,
              private serviceProduct: ProductosService,
              private servicePedido: PedidosService) 
  {
    this.valorFiltro = "";
    this.showFiltro = false; 
    this.totalIva = 0;
    // this.creditos = this.servicePedido.getCreditos();
  }

  ngOnInit() 
  {  

    setTimeout(() => {                
      this.listProducts = this.productosDelPedido.data     
      // console.log("---->", this.productosDelPedido);
      if( this.listProducts != undefined){ //Editar pedido
        this.searchNomberdeProductos(this.listProducts)
        this.totalPedidoEdit = this.calculeSumSubTotal()
        this.lengthListProducts = this.listProducts.length;
        this.TypeFormulario = true;

      }else{ // Create pedido
        this.listProducts = this.productosDelPedido.data = []
        this.lengthListProducts = 0;
      }    
      // console.log( this.listProducts)
      //Obtener lista de productos disponibles
      this.serviceProduct.getProducts(this.idCliente).subscribe(
        res => {
          this.listProductos = res.data.productos;
          this.initRenderizarSelect();
        },
        err => {
          console.log(err);
        })  
   }, 1000);   
    
   // Validación de los campos de un pedido 
    this.newProduct = this.fb.group({
      id: [null, Validators.required],
      avatar: [''],
      nombre: [''],
      unidadCompra: [''],
      cantidad: [1, [Validators.required, Validators.minLength(1)]],
      precioUnidad: [],
      subtotal:[0]
    })
  }

  ngAfterContentInit()
  {
    this.disabledClass = true;  
    
  }  

  /**
   * Busca los nombres correspondientes a cada producto y lo agrega 
   * a la variable de nombre de cada producto.
   * @param listProducts 
   */
  public searchNomberdeProductos(listProductos){
    listProductos.map(producto => {
      producto.nombre = producto.nodoproducto.materiaproductos.nombre
    })
  }

  /**
  * Obtiene la unidad de compra, avatar y precio del producto seleccionado
  * 
  * @view Create/Edit Product
  * @return {object}  
  **/
  public onChangeProduct(id_product){
    this.listProductos.filter((element) => {
      
       if(element.id == id_product.value){
           if(!element.maestroproductos)
           {
               var nombre = element.materiaproductos.nombre
               var unidadCompra = element.materiaproductos.unidad_compra;
               var avatar = element.materiaproductos.imagen;
           }
           else
           {
               var nombre = element.maestroproductos.nombre
               var unidadCompra = element.maestroproductos.unidad_compra;
               var avatar = element.maestroproductos.imagen;
               this.maestroProducto = true;
           }
           this.newProduct.patchValue({
              avatar: avatar,
              nombre: nombre,
              unidadCompra: unidadCompra,
              precioUnidad: element.maestroprecio[0].precio
           })
       }
    })
  }

  /**
  * Renderizar elementos select luego de obtener los datos
  * para el blucle ngfor.
  *  
  * @view Crear y Show pedido
  * @return {void}
  **/
  initRenderizarSelect(){
    setTimeout(function(){
      var elems = document.querySelectorAll('select');
      M.FormSelect.init(elems);
    })
  }

  /**
   * Retorna un objeto de tipo producto usando la arquitectura 
   * especificada desde la Api
   * 
   * @view All
   * @returns {Product}
   */
  returnProductSerializado(){
    return {
      id: this.newProduct.value.id ,
      nodoproducto: {
                      maestroproductos: null,
                        // {
                        //   Aviso: 'No es un formato de compras'
                        // }, 
                      materiaproductos: 
                        { 
                          imagen: this.newProduct.value.avatar,
                          nombre: this.newProduct.value.nombre,
                          unidad_compra: this.newProduct.value.unidadCompra
                        }
                    },
      avatar: this.newProduct.value.avatar,
      nombre: this.newProduct.value.nombre,
      cantidad: this.newProduct.value.cantidad,
      precio: {precio: this.newProduct.value.precioUnidad},
      total: Math.round(this.newProduct.value.subtotal)
    }
  }

  /**
   * Emitir evento de envio de formulario
   * 
   * @return {void}
   */
  enviar() 
  {
    this.enviarFormulario.emit(true);
  }

  /**
   * Cambiar cantidad del producto por teclado.
   * 
   * @return {void}
   */
  inputUpdateCantidad(i){
    if(!this.tipoDeFormulario){
      this.calculePricesProduct(this.listProducts[i].cantidad, i)
    }
  }
}
