import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-resumen-dia',
  templateUrl: './resumen-dia.component.html',
  styleUrls: ['./resumen-dia.component.scss']
})
export class ResumenDiaComponent implements OnInit {

  @Input('totalPedidos') totalPedidos: number;
  @Input('mediaPorPedido') mediaPorPedido: number;
  @Input('cantidadDePedidos') cantidadDePedidos: number;

  constructor() { }

  ngOnInit() {
  }

}
