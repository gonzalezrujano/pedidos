import { Component, OnInit, Input } from '@angular/core';
import { Pedido } from '../../../models/Pedido';

@Component({
  selector: 'app-carta-pedido',
  templateUrl: './carta-pedido.component.html',
  styleUrls: ['./carta-pedido.component.scss']
})
export class CartaPedidoComponent implements OnInit {

  @Input('pedido') pedido: Pedido;
  
  tipoDeFormulario : boolean = false;

  constructor() {}
  
  ngOnInit() {
    let fecha_Actual =  this.formatFecha(new Date())    
    this.tipoDeFormulario = (this.pedido.fecha_entrega >= fecha_Actual)
    // console.log( this.tipoDeFormulario )
  }

  /**
  * Apartir de un dato de tipo Date, se retorna un string formateado a 'YYYY-MM-dd'.
  *
  * @param date Un dato de tipo Object-Date, que contiene una fecha
  * @return string
  */
  public formatFecha(fecha){
    return fecha.getFullYear()+'-'+(fecha.getMonth()<10?'0':'')+(fecha.getMonth() + 1)+'-'+(fecha.getDate()<10?'0':'')+fecha.getDate()
    
  }

}
