import { Component, OnInit } from '@angular/core';
import { Pedido } from '../../../models/Pedido';
import { Cliente } from '../../../models/Cliente';

import { pedidos } from '../../../_data/pedidos';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ClientesService } from '../../../services/cliente/clientes.service';
import { clientes } from '../../../_data/clientes'
import { PedidosService } from '../../../services/pedido/pedidos.service';
import { fecha } from '../../../_data/fecha';
import { Builder } from 'protractor';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var M: any;
@Component({
  selector: 'app-list-pedidos',
  templateUrl: './list-pedidos.component.html',
  styleUrls: ['./list-pedidos.component.scss'],
  animations: [
    trigger('mostrarFiltro', [
      state('presenteIzquierdo', style({
        opacity: 1
      })),
      state('ocultoIzquierdo', style({
        transform: 'translateX(-150%)',
        opacity: 0
      })),
      state('presenteDerecho', style({
        opacity: 1,
        left: '7px'
      })),
      state('ocultoDerecho', style({
        transform: 'translateX(150%)',
        opacity: 0
      })),
      transition('* => ocultoIzquierdo, * => presenteIzquierdo, * => ocultoDerecho, * => presenteDerecho', [
        animate('0.2s')
      ])
    ]),
  ]
})
export class ListPedidosComponent implements OnInit {
  fecha  = fecha;
  /**
   * Id para refrescar el setInterval de los datos del pedido del cliente
   * 
   * @property {number}
   */
  refreshIntervalId

  /**
   * Valor del campo de filtro de numero de orden
   * 
   * @property {string}
   */
  valorFiltroOrden: string = '';

  /**
   * Valor del campo de filtro del nombre del cliente
   * 
   * @property {string}
   */
  valorFiltroCliente: string = '';

  /**
   * Valor del campo de filtro de fecha de entrega
   * 
   * @property {string}
   */
  valorFiltroFecha: string = '';
  
  /**
   * Visibilidad del campo de filtro de numero de orden
   * 
   * @property {boolean}
   */
  filtrarPorOrden: boolean = false;

  /**
   * Visibilidad del campo de filtro del nombre del cliente
   * 
   * @property {boolean}
   */
  filtrarPorCliente: boolean = false;

  /**
   * Visibilidad del campo de filtro de fecha de entrega
   * 
   * @property {boolean}
   */
  filtrarPorFecha: boolean = false;  

   /**
   * Listado auxiliar de pedidos del dia para la vista card
   * 
   * @property {Array<Pedido>}
   */
  pedido: Array<Pedido>;

  /**
   * Listado de pedidos del dia
   * 
   * @property {Array<Pedido>}
   */
  listadoDePedidos: Array<Pedido>;

  /**
   * Listado de clientes consultados a la api del dia
   * 
   * @property {Array<Cliente>}
   */
  listadoDeCliente: Array<Cliente>;

  /**
   * Listado de pedidos filtrados
   * 
   * @property {Array<Pedido>}
   */
  listadoDePedidosFiltrados: Array<Pedido> = [];

  /**
   * Cantidad de pedidos de la fecha seleccionada
   * 
   * @property {number}
   */
  cantidadDePedidos: number = this.calcularCantidadDePedidos();

  /**
   * Sumatoria total de pedidos de la fecha seleccionada
   * 
   * @property {number}
   */
  totalPedidos: number = this.calcularTotalDePedidos();

  /**
   * Precio medio por pedido
   * 
   * @property {number}
   */
  mediaPorPedido: number = this.calcularMediaPorPedido();

  /**
   * Precio medio por pedido
   * 
   * @property {Array<Pedido>}
   */
  tempPedidos;

  /**
   * Mostrar campo de filtro de norden y ocultar el resto
   * 
   * @return {void}
   */
  public verFiltroPorNorden() : void
  {
    this.filtrarPorOrden = true;
    this.filtrarPorCliente = false;
    this.filtrarPorFecha = false;

    this.valorFiltroCliente = "";
    this.valorFiltroFecha = "";    
  }

  /**
   * Mostrar campo de filtro de cliente y ocultar norden
   * 
   * @return {void}
   */
  public verFiltroPorCliente() : void
  {
    this.filtrarPorOrden = false;
    this.filtrarPorCliente = true;

    this.valorFiltroOrden = "";
  }
  
  /**
   * Mostrar campo de filtro de fecha de entrega y ocultar el norden
   * 
   * @return {void}
   */
  public verFiltroPorFecha() : void
  {
    this.filtrarPorOrden = false;
    this.filtrarPorFecha = true;
    this.valorFiltroOrden = "";
  }

  /**
   * Filtrar listado de pedidos por parametros
   * 
   * @return {void}
   */
  public filtrarPedidos() : void
  {    
    //Inicializar las variables de la consulta searchApiPedidoDateVacia
    if(this.valorFiltroFecha == "" ){
      // clearInterval(this.refreshIntervalId)      
      this.initVariables()          
    }

    //Añadir la fecha existenteantes
    if( this.valorFiltroOrden == "" && this.valorFiltroFecha == "")
    this.existeFiltroFecha()
    
    if(this.Globals.pedidos.data.length != 0  || !(this.valorFiltroFecha == "")){
      //Filtrar datos globales          
      this.listadoDePedidosFiltrados= this.Globals.pedidos.data.filter((pedido) => {
        return pedido.norden.toUpperCase().includes(this.valorFiltroOrden.toUpperCase()) && pedido.usuario.name.toUpperCase().includes(this.valorFiltroCliente.toUpperCase());
      });     
      this.calcularResumenDePedidos() 
      this.Globals.datosCargados = true;      

    }  
   
    // else if( !(this.valorFiltroFecha == "") || ( !(this.valorFiltroCliente == "") && !(this.valorFiltroFecha == "")) ){
    // // Filtrar datos del pedido solicitado a la API 
    //   this.searchApiPedidoDate(); 
    //   // this.startIntervalFecha();

    // }
  }
  public filtrarPedidosfecha() : void
  {
    this.fecha[0] = this.valorFiltroFecha //Almacenar fecha seleccionada

    if( !(this.valorFiltroFecha == "")  ){
      // Filtrar datos del pedido solicitado a la API 
        this.searchApiPedidoDate(); 
        // this.startIntervalFecha();
    }else{
      this.searchApiPedidoDate(); 
    }
  }

  /**
   * Calcular cantidad de pedidos filtrados por el usuario
   * 
   * @return {void}
   */
  public calcularResumenDePedidos() : void
  {
    this.cantidadDePedidos = this.calcularCantidadDePedidos();    
    this.totalPedidos = this.calcularTotalDePedidos();
    this.mediaPorPedido = this.calcularMediaPorPedido();
  }

  /**
   * Calcular cantidad de pedidos filtrados por el usuario
   * 
   * @return {number}
   */
  private calcularCantidadDePedidos() : number
  {
    return this.listadoDePedidosFiltrados.length;
  }

  /**
   * Calcular ingreso bruto en pedidos filtrados
   * 
   * @return {number}
   */
  private calcularTotalDePedidos() : number
  {
    let sumatoria: number = 0;

    this.listadoDePedidosFiltrados.map((pedido) => (sumatoria += pedido.total));

    return  Math.round(sumatoria);
  }

  /**
   * Calcular precio medio por pedido
   * 
   * @return {number}
   */
  private calcularMediaPorPedido() : number
  {
    return (this.cantidadDePedidos != 0) ?  Math.round(this.totalPedidos / this.cantidadDePedidos) : 0;
  }
   

  /**
   * Inicializar las variables de la peticion que se realiza constante
   * y las variables globales de los pedidos.
   * 
   * @return {any}
   */
  public initVariables(){    
    this.Globals.pedidos = this.tempPedidos;
    this.Globals.datosCargados = true;      
    this.pedido = this.Globals.pedidos.data;
    this.listadoDePedidos = this.Globals.pedidos.data;
    this.listadoDePedidosFiltrados =  this.listadoDePedidos;   
    // Si existen resultados del dia calcular total
    if (this.Globals.pedidos.data.length != 0){
      this.calcularResumenDePedidos() 
    }else{
      this.cantidadDePedidos = 0    
    }
  }

  /**
   * Consultar la Api e Inicializar la variables del componente
   * y las variables globales de los pedidos si no existe fecha.
   * 
   * @return {any}
   */
  public searchApiPedidoVacio(): any {
    this.servicePedidos.getPedido('').subscribe(res =>{
      // console.log("starpai___",res)
      this.tempPedidos = res;    
   });
  }

  /**
   * Consultar la Api e Inicializar la variables del componente
   * y las variables globales de los pedidos si existe fecha.
   * 
   * @return {any}
   */
  public searchApiPedidoDate(): any {
    this.Globals.datosCargados = false;

    if (this.valorFiltroCliente == "")
    this.servicePedidos.getPedido(this.valorFiltroFecha).subscribe(res =>{
      
      this.Globals.datosCargados = true;
      this.Globals.pedidos = res;      
      this.pedido = res.data;
      this.listadoDePedidos = res.data;
      this.listadoDePedidosFiltrados = res.data;   
      //Si existe cliente para filtrar
      // if( !(this.valorFiltroCliente == ""))
      // {
      //   console.log("hola2")

      //   this.listadoDePedidosFiltrados = this.listadoDePedidos.filter((pedido) => {
      //     return pedido.usuario.name.includes(this.valorFiltroCliente) && pedido.fecha_entrega.includes(this.valorFiltroFecha);
      //   });
      // }
      // this.initDatePicker()
      this.calcularResumenDePedidos()        
    });
  }

  /**
   * Comenzar las peticiones progresivas para mantener 
   * actualizado el listado de pedidos del cliente
   * sin fecha especifica.
   * 
   * @return {any}
   */
  public startIntervalFechaVacia()
  {
    this.searchApiPedidoVacio()    
    setInterval( () => {
      this.searchApiPedidoVacio()
    },10000); 
   
  }

  /**
   * Comenzar las peticiones progresivas para mantener 
   * actualizado el listado de pedidos del cliente
   * con una fecha especifica.
   * 
   * @return {any}
   */
  public startIntervalFecha()
  {
    this.refreshIntervalId = setInterval( () => {
      this.searchApiPedidoDate()
    },10000); 
  }

  /**
   * Verifica si existe fecha filtrada, y filtra los pedidos
   * 
   * @return {void}
   */
  existeFiltroFecha(){    
      this.fecha[0] = fecha[0] 
      this.valorFiltroFecha =  this.fecha[0] 
      this.filtrarPedidosfecha();  
  }

  /**
   * Inicializar el filtro de fecha por la fecha actual.
   * 
   * @return {void}
   */
  initFiltroFecha(){
    let start_date = new Date()    
    let parse_date = this.formatFecha(start_date)
    this.valorFiltroFecha  = '';
    this.filtrarPedidosfecha();    
  }

  /**
  * Apartir de un dato de tipo Date, se retorna un string formateado a 'YYYY-MM-dd'.
  *
  * @param date Un dato de tipo Object-Date, que contiene una fecha
  * @return string
  */
  public formatFecha(fecha){
    return fecha.getFullYear()+'-'+(fecha.getMonth()<10?'0':'')+(fecha.getMonth() + 1)+'-'+(fecha.getDate()<10?'0':'')+fecha.getDate()  
  }

  /**
   * 
   */
  public initDatePicker(){
    var elemss = document.querySelectorAll('.datepicker');
    M.Datepicker.init(elemss, {format: 'yyyy-mm-dd'});
  }

  public salir()
  {
    window.location.href = "/";
  }

  constructor(private servicePedidos: PedidosService,
              public Globals: pedidos,
              public fb: FormBuilder)
  {  
    if(!fecha[0]){
      this.initFiltroFecha()
    }else{      
      this.existeFiltroFecha()  
    }

  }
  
  ngOnInit() 
  {      
    this.searchApiPedidoVacio()    
    // this.startIntervalFechaVacia()
  }
  
  ngAfterViewInit(){
  //  setTimeout(() => {
  //   this.initDatePicker()
  //  }, 2000);
  }
 
}
