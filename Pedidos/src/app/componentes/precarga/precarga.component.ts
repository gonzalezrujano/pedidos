import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-precarga',
  template: `<div class="d-flex justify-content-center align-items-center" id="m-height">
                <div class="loader"></div>
            </div>`,
  styleUrls: ['./precarga.component.scss']
})
export class PrecargaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
