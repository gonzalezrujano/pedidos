import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  template: `
    <!-- Navbar -->
    <nav class="green darken-1">
        <div class="nav-wrapper">
            <a 
              routerLink="/"
              class="brand-logo waves-effect left button button_back"
            >
              <i class="material-icons">arrow_back_ios</i>
            </a>
            <a class="brand-logo center title-navbar">{{ titulo }}</a>
            <a *ngIf="colocarBotonEnviar" title="Enviar pedido" (click)="enviar()" class="brand-logo waves-effect right button button_send"><i class="material-icons">send</i></a>
        </div>
    </nav>
  `,
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  constructor() {}
 /**
   * Titulo de la vista
   * 
   * @property {string}
   */
  @Input('titulo') titulo: string;

  /**
   * Enlace de regreso
   * 
   * @property {string}
   */
  @Input('enlace') enlace: string;

  /**
   * ¿La vista es un formulario?
   * 
   * @property {boolean}
   */
  @Input('colocarBotonEnviar') colocarBotonEnviar?: boolean;

  /**
   * Evento de envio de formulario
   * 
   * @property {EventEmitter<boolean>}
   */
  @Output() enviarFormulario = new EventEmitter<boolean>(); 

  ngOnInit() {}

  /**
   * Emitir evento de envio de formulario
   * 
   * @return {void}
   */
  enviar() 
  {
    console.log('Formulario enviado');
    this.enviarFormulario.emit(true);
  }

}
