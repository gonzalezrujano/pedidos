import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro',
  pure: false
})
export class Filtro implements PipeTransform {

  transform(value: any[], valorFiltro: string,  prop?: any): any {
    if(!value){
    return [];
    }

    if(!valorFiltro || !prop){
    return value;
    }

    const _valorFiltro = valorFiltro.toLowerCase(),
    _isArr = Array.isArray(value),
    _flag = _isArr && typeof value[0] === 'object' ? true : _isArr

    return value.filter(ele => {
    let val = _flag ? ele[prop]:ele;
    return val.toString().toLowerCase().includes(_valorFiltro);
    });
  }
}
