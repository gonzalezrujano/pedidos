import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPedidosComponent } from './componentes/home/list-pedidos/list-pedidos.component';
import { FormPedidosComponent } from './componentes/pedido/form-pedidos/form-pedidos.component';
import { NgSelectModule, NgSelectConfig } from '@ng-select/ng-select';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'pedidos',
		pathMatch: 'full'
	},
	{
		path: 'pedidos',
		component: ListPedidosComponent,
		data: {animation: 'Pedidos'}
	},
	{
		path: 'crear',
		component: FormPedidosComponent,
		data: {animation: 'Crear'}
	},
	{
		path: 'editar/:id',
		component: FormPedidosComponent,
		data: {animation: 'Editar'}
	},
	{
		path: 'ver/:id',
		component: FormPedidosComponent,
		data: {animation: 'Editar'}
	}				
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
	constructor(private config: NgSelectConfig) {
		this.config.notFoundText = 'No existe';
	}
}
