import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URI } from '../apiUrl';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  constructor(private http: HttpClient) { }

  guardarToken(token: string): void{
    localStorage.setItem('usuarioToken', token)
  }
 
  obtenerTokenApi()
  {
    return this.http.get(`${API_URI}/develop`);
  }
}
