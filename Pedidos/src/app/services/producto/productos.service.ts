import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_URI } from '../apiUrl'

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  private headers;
  constructor(private http: HttpClient) { 
        this.headers = new Headers({  'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
  }

  /**
   * Funcion que comunica con la api y retorna los productos
   * de un cliente segun su ID
   * @param id 
   * @return {Observable}
   */
  getProducts(id): Observable<any>{
    return this.http.get(`${API_URI}/productos/create?clienteId=${id}`, {headers : this.headers });
  }

}
