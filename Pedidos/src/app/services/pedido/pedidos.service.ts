import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_URI } from '../apiUrl'
import { pedidos } from '../../_data/pedidos'
import { creditos } from '../../_data/products'
@Injectable({
  providedIn: 'root'
})
export class PedidosService {
  private headers;

 constructor(private http: HttpClient) {
    this.headers = new Headers({  
                                'Content-Type': 'application/json', 
                                'Accept': 'application/json',
                                'Access-Control-Allow-Origin': '*',
                              });
  }

  getCreditos(){
    return pedidos.creditos[0]
  }

  saveCreditos(credito){    
    if(!credito){
      pedidos.creditos[0] = 0
    }else{
      pedidos.creditos[0] = credito
    }
  }

  getPedidos():Observable<any>{
    return this.http.get(`${API_URI}/pedidos`);
  }

  getPedido(pedido):Observable<any>{
  return this.http.get(`${API_URI}/pedidos/create?fecha_entrega=${pedido}`,pedido);
  }

  getObtenerProductosPedido(norden): Observable<any>
  {
    return this.http.get(`${API_URI}/pedidos/${norden}`);
  }

  obtenernOrden(id)
  {
    return this.http.get(`${API_URI}/norden?usuarioId=${id}`, {headers : this.headers });
  }

  createPedido(pedido){
    
  	return this.http.post(`${API_URI}/pedidos`, pedido, {headers : this.headers});
  }

  updatePedido(id,pedido){
  	return this.http.put(`${API_URI}/pedidos/${id}`, pedido);
  }
}





