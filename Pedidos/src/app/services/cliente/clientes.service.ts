import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_URI } from '../apiUrl'
import { clientes} from '../../_data/clientes'

@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  private headers;

  constructor(public http: HttpClient) 
  { 
    this.headers = new Headers({  'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
  }

  public obtenerClientes(): Observable<any>
  {   
    return this.http.get(`${API_URI}/clientes`, {headers : this.headers })
  }
}
